# dottAssignment
To build application import project into Atmel Studio 7
with the file "helloDott.atsln"

Build and Program the Atmel SAME54 Xplained Pro through 
Atmel Studio with a USB cable attached to the Debug
USB port on the board.

To see the output of the application use an FDTI board
Connect GND between the two boards and the Atmel SAME54 UART TX
pin EXT HEADER 1 pin 14(PA04) to the RX on the FDTI board
Open a terminal on your computer
baud rate 9600
8 data bits
1 stop bits
no pariry