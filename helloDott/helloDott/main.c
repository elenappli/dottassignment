#include <atmel_start.h>
#include "atmel_start_pins.h"

#include "FreeRTOS.h"
#include "task.h"
#include "hal_rtos.h"

/* 
 * Picked a sane default as the task only has a for loop with 
 * a couple hal and rtos function calls .
 * If I had multiple task in application I would consider total
 * chip memory with the number of application and use the FreeRtos
 * uxTaskGetStackHighWaterMark()  to optimize memory per task
*/
#define TASK_HELLO_STACK_SIZE (256 / sizeof(portSTACK_TYPE))
/*
 Pick priority one higher than idle task priority  
 */
#define TASK_HELLO_STACK_PRIORITY (tskIDLE_PRIORITY + 1)

static TaskHandle_t      xCreatedHelloTask;
static uint8_t hello_dott[12] = "Hello Dott!!";

/** @brief Os task to sends Hello Message
 *
 * Send Hello Message over UART0, once a second
 * Also toggle LED0 for visual confirmation 
 * task is running
 *
 * @param p - task arguments not used
 */
static void task_hello(void *p)
{
	(void)p;
	for (;;) {
		gpio_toggle_pin_level(LED0);
		io_write(&USART_0.io, hello_dott, 12);
		os_sleep(1000);
	}
}

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();

	if (xTaskCreate(task_hello, "Hello", TASK_HELLO_STACK_SIZE, NULL, TASK_HELLO_STACK_PRIORITY, &xCreatedHelloTask) != pdPASS)
		return -1;

	usart_sync_enable(&USART_0);

	vTaskStartScheduler();

	return 0;
}
